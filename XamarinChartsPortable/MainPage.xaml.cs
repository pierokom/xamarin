﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Microcharts;
using Entry = Microcharts.Entry;
using SkiaSharp;

namespace XamarinChartsPortable
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        List<Entry> _entris = new List<Entry>()
        { 
            new Entry(3)
            {
                Label = "Windows",ValueLabel = "3%",Color = SKColor.Parse("#2c3e50")
            },
            new Entry(55)
            {
                Label = "Android",ValueLabel = "55%",Color = SKColor.Parse("#77d065")
            },
            new Entry(50)
            {
                Label = "iOS",ValueLabel = "50%",Color = SKColor.Parse("#b455b6")
            },
            new Entry(2)
            {
                Label = "others",ValueLabel = "2%",Color = SKColor.Parse("#3498db")
            },

        };
        public MainPage()
        {
            InitializeComponent();
            ChartV.Chart = new BarChart()
            {
                PointMode = PointMode.Square,
                PointSize = 40,
                MinValue = 0,
                MaxValue = 100,
                LabelTextSize = 40,
                Margin = 40,
                Entries = _entris,
                BackgroundColor = SKColor.Parse("#e1e7e7")
            };
        }
    }
}
